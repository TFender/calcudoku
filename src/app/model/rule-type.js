"use strict";
(function (RuleType) {
    RuleType[RuleType["ADD"] = 0] = "ADD";
    RuleType[RuleType["SUBTRACT"] = 1] = "SUBTRACT";
    RuleType[RuleType["MULTIPLY"] = 2] = "MULTIPLY";
    RuleType[RuleType["DIVIDE"] = 3] = "DIVIDE";
})(exports.RuleType || (exports.RuleType = {}));
var RuleType = exports.RuleType;
