import {RuleType} from "./rule-type";
import {Coordinate} from "./coordinate";

export class Rule {
    type: RuleType;
    amount: number;
    coordinates: Coordinate[];

    constructor(type: RuleType, amount: number, coordinates: Coordinate[]) {
        this.type = type;
        this.amount = amount;
        this.coordinates = coordinates;
    }
}