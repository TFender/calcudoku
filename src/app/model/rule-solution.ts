import {Coordinate} from "./coordinate";
import {Rule} from "./rule";
export class RuleSolution {

  private rule: Rule;
  private coordinateValueMap: Map<Coordinate,number>;

  constructor(rule: Rule) {
    this.rule = rule;
    this.coordinateValueMap = new Map();
  }

  add(coordinate: Coordinate, value: number) {
    this.coordinateValueMap.set(coordinate, value);
  }

  getAll(): IterableIterator<[Coordinate,number]> {
    return this.coordinateValueMap.entries();
  }

  public checkIfCoordinateExists(coor: Coordinate): boolean {
    return this.coordinateValueMap.has(coor);
  }

  public getAllValues(): string {
    let s: string ="";
    this.coordinateValueMap.forEach((value: number, key: Coordinate) => {
      s += "" + value;
    });
    return s;
  }

  clone(): RuleSolution {
    let clone = new RuleSolution(this.rule);
    this.coordinateValueMap.forEach((value, coordinate) => clone.add(coordinate, value));
    return clone;
  }

  getRule() {
    return this.rule;
  }

  print() {
    console.log(this.rule);
    this.coordinateValueMap.forEach((value, coordinate) => console.log(coordinate + " " + value));
  }
}
