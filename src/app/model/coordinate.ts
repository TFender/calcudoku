export class Coordinate {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  equals(coordinate: Coordinate): boolean {
    if (this.x === coordinate.x && this.y === coordinate.y) return true;
    else return false;
  }


  toString(): string {
    return "x:" + this.x + ", y:" + this.y;
  }

}
