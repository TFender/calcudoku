import {Coordinate} from "./coordinate";
import {AppService} from "../app.service";

export class CoordinateWrapper {

    coordinate: Coordinate;
    text: string = "";
    solutionOptions: string[] = ["No Options"];
    amount: number;
    rule: string = "  ";
    color: string = "";
    clickable: boolean = true;

    border = {
        left: "",
        right: "",
        bottom: "",
        top: ""
    }

    private selected: boolean = false;

    constructor(coordinate: Coordinate){
        this.coordinate = coordinate;
    }

    select() {
        if (this.clickable) {
            this.selected = true;
        }
    }

    unselect() {
        this.selected = false;
    }

    setColor(color: string): void {
        this.color = color;
    }

    reset() {
        this.color = "";
        this.text = "";
        this.clickable = true;
        this.rule = "  ";
        this.amount = 0;
        this.solutionOptions = ["No Options"];
        this.unselect();
        this.setBordersGrey();
    }

    setAndDisplayNumber(amount: number){
        this.amount = amount;
        this.text = "" + amount;

    }

    isSet() {
        this.clickable = false;
    }

    setBorderBlack(direction: string) {
        if (direction == 'left') {
            this.border.left = "solid #262728";
        }
        if (direction == 'right') {
            this.border.right = "solid #262728";
        }
        if (direction == 'bottom') {
            this.border.bottom = "solid #262728";
        }
        if (direction == 'top') {
            this.border.top = "solid #262728";
        }
    }

    setBordersGrey() {
        this.border.left = "solid #82868c";
        this.border.right = "solid #82868c";
        this.border.bottom = "solid #82868c";
        this.border.top = "solid #82868c";
    }

    setSolutionOptions(){
    }
}
