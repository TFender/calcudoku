import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {Rule} from "./model/rule";
import {RuleType} from "./model/rule-type";
import {RuleSolution} from "./model/rule-solution";
import {Coordinate} from "./model/coordinate";
import {letProto} from "rxjs/operator/let";

@Injectable()
export class AppService {

    dimension: number;
    solutionMatrix: number[][] = [];
    rules: Rule[] = [];
    ruleSolutions: RuleSolution[][] = [];
    ruleSolutionIndex: number;
    amountOfCoordinatesCovered: number = 0;
    allCoordinatesCovered: boolean = false;
    publishedSolution: Subject<number[][]> = new Subject<number[][]>();

    constructor() {
        this.setEmptyMatrixWithGivenDimension(4);
    }

    setEmptyMatrixWithGivenDimension(dimension: number): void {
        this.dimension = dimension;
        this.solutionMatrix = [];
        for (let i = 0;i<dimension;i++) {
            this.solutionMatrix.push(new Array(this.dimension))
        }
        this.publishSolution();
        this.solutionMatrix = [];
        this.rules = [];
        this.ruleSolutions = [];
        this.amountOfCoordinatesCovered = 0;
        this.allCoordinatesCovered = false;
    }

    publishSolution(): void {
        this.publishedSolution.next(this.solutionMatrix);
    }

    addRule(newRule: Rule): void {
        let rulesCopy = Array.from(this.rules);
        rulesCopy.forEach(rule => {
            let coordinatesCopy = Array.from(rule.coordinates);
            coordinatesCopy.forEach(coordinate => {
                newRule.coordinates.forEach(newCoordinate => {
                    if(coordinate.equals(newCoordinate)){
                        this.rules = this.removeItemFromArray(this.rules,coordinate);
                        this.amountOfCoordinatesCovered--;
                    }
                })
            })
        })
        this.rules.push(newRule);
        newRule.coordinates.forEach(() => this.amountOfCoordinatesCovered++);
        this.checkIfAllCoordinatesCoveredByRule();
    }

    private removeItemFromArray(array: Array<any>, item): Array<any> {
        let indexOfItem: number = array.indexOf(item);
        return array.slice(0,indexOfItem).concat(array.slice(item+1));
    }

    private checkIfAllCoordinatesCoveredByRule(): void {
        if(this.amountOfCoordinatesCovered === this.dimension * this.dimension) this.allCoordinatesCovered = true;
        else this.allCoordinatesCovered = false;
    }

    solve(): void {
        if(!this.allCoordinatesCovered) {
            console.log("Not all coordinates covered");
            return;
        }
        this.prepareRuleSolutions();
        this.getSolutionUsingRuleSolutions();
    }

    private getSolutionUsingRuleSolutions(){
        this.combineRuleSolutions(0,null);
        this.publishSolution();
    }

    private combineRuleSolutions(ruleSolutionIndex: number, combinedSolution: RuleSolution) {
        if(!combinedSolution) combinedSolution = new RuleSolution(undefined);
        if(!this.isCombinedSolutionCompliant(combinedSolution)) return;
        if(ruleSolutionIndex === this.ruleSolutions.length) return this.putSolutionInMatrix(combinedSolution);
        for(let i = 0; i < this.ruleSolutions[ruleSolutionIndex].length; i++) {
            combinedSolution = combinedSolution.clone();
            let coordinateValueMap = this.ruleSolutions[ruleSolutionIndex][i].getAll();
            let coordinateValuePair;
            while(!(coordinateValuePair = coordinateValueMap.next()).done) {
                combinedSolution.add(coordinateValuePair.value[0],coordinateValuePair.value[1]);
            }
            this.combineRuleSolutions(ruleSolutionIndex+1,combinedSolution);
        }
    }

    private isCombinedSolutionCompliant(combinedSolution: RuleSolution): boolean {
        let coordinateValueMap = combinedSolution.getAll();
        let coordinateValuePair;
        let coordinateValueArray = [];
        let isRuleCompliant: boolean = true;
        while(!(coordinateValuePair = coordinateValueMap.next()).done) {
            coordinateValueArray.push(coordinateValuePair.value);
        }
        if(!coordinateValueArray) return true;
        coordinateValueArray.forEach((pair) => {
            coordinateValueArray.forEach((otherPair) => {
                if(pair[1]==otherPair[1]){
                    if(!pair[0].equals(otherPair[0])){
                        if(pair[0].x===otherPair[0].x) isRuleCompliant = false;
                        else if (pair[0].y===otherPair[0].y) isRuleCompliant = false;
                    }
                }
            })
        });
        return isRuleCompliant;
    }

    private putSolutionInMatrix(combinedSolution: RuleSolution) {
        let matrix = [];
        for(let i = 0; i < this.dimension; i++) matrix.push([]);
        for(let i = 0; i < this.dimension; i++) {
            for(let j = 0; j < this.dimension; j++) {
                matrix[i].push(undefined);
            }
        }
        let coordinateValueMap = combinedSolution.getAll();
        let coordinateValuePair;
        while(!(coordinateValuePair = coordinateValueMap.next()).done) {
            matrix[coordinateValuePair.value[0].x - 1][coordinateValuePair.value[0].y - 1] = coordinateValuePair.value[1];
        }
        this.solutionMatrix = matrix;
        this.publishSolution();
    }

    public prepareRuleSolutions() {
        this.ruleSolutions = [];
        for(let i = 0; i < this.rules.length; i++) {
            this.ruleSolutions.push([]);
            this.ruleSolutionIndex = i;
            this.tryAndAddRuleSolutions(i,0,null);
        }
    }

    private tryAndAddRuleSolutions(ruleIndex: number, coordinateIndex: number, ruleSolution: RuleSolution) {
        if(!ruleSolution) ruleSolution = new RuleSolution(this.rules[ruleIndex]);
        if(coordinateIndex === this.rules[ruleIndex].coordinates.length) return this.checkAndAddRuleSolution(ruleSolution);
        let coordinates = this.rules[ruleIndex].coordinates[coordinateIndex];
        for(let i = 1; i <= this.dimension; i++) {
            ruleSolution = ruleSolution.clone();
            ruleSolution.add(coordinates,i);
            this.tryAndAddRuleSolutions(ruleIndex,coordinateIndex+1,ruleSolution);
        }
        return;
    };

    private checkAndAddRuleSolution(ruleSolution: RuleSolution): any {
        switch (ruleSolution.getRule().type) {
            case RuleType.ADD       :return this.checkAndAddRuleSolutionAddition(ruleSolution);
            case RuleType.SUBTRACT  :return this.checkAndAddRuleSolutionSubtraction(ruleSolution);
            case RuleType.MULTIPLY  :return this.checkAndAddRuleSolutionMultiplication(ruleSolution);
            case RuleType.DIVIDE    :return this.checkAndAddRuleSolutionDivision(ruleSolution);
        }
    }

    private checkAndAddRuleSolutionAddition(ruleSolution: RuleSolution) {
        let sum = 0;
        let coordinateValueMap = ruleSolution.getAll();
        let coordinateValuePair: IteratorResult<[Coordinate,number]>;
        while(!(coordinateValuePair = coordinateValueMap.next()).done) {
            sum = sum + coordinateValuePair.value[1];
        }
        if(sum === ruleSolution.getRule().amount){
            this.addRuleSolutionIfRowColumnConditionHolds(ruleSolution);
            return;
        }
        else return;
    }

    private checkAndAddRuleSolutionSubtraction(ruleSolution: RuleSolution) {
        let result;
        let goal = ruleSolution.getRule().amount;
        let coordinateValueMap = ruleSolution.getAll();
        let coordinateValuePair;
        let coordinateValueArray = [];
        let isRuleCompliant: boolean = false;
        while(!(coordinateValuePair = coordinateValueMap.next()).done) {
            coordinateValueArray.push(coordinateValuePair.value);
        }
        coordinateValueArray.forEach(pair => {
            result = pair[1];
            coordinateValueArray.forEach(otherPair => {
                if(!pair[0].equals(otherPair[0])){
                    result = result - otherPair[1];
                }
            })
            if(result === goal) isRuleCompliant = true;
        })
        if(isRuleCompliant) this.addRuleSolutionIfRowColumnConditionHolds(ruleSolution);
    }

    private checkAndAddRuleSolutionMultiplication(ruleSolution: RuleSolution) {
        let product = 1;
        let coordinateValueMap = ruleSolution.getAll();
        let coordinateValuePair: IteratorResult<[Coordinate,number]>;
        while(!(coordinateValuePair = coordinateValueMap.next()).done) {
            product = product * coordinateValuePair.value[1];
        }
        if(product === ruleSolution.getRule().amount){
            this.addRuleSolutionIfRowColumnConditionHolds(ruleSolution);
            return;
        }
        else return;
    }

    private checkAndAddRuleSolutionDivision(ruleSolution: RuleSolution) {
        let result;
        let goal = ruleSolution.getRule().amount;
        let coordinateValueMap = ruleSolution.getAll();
        let coordinateValuePair;
        let coordinateValueArray = [];
        let isRuleCompliant: boolean = false;
        while(!(coordinateValuePair = coordinateValueMap.next()).done) {
            coordinateValueArray.push(coordinateValuePair.value);
        }
        coordinateValueArray.forEach(pair => {
            result = pair[1];
            coordinateValueArray.forEach(otherPair => {
                if(!pair[0].equals(otherPair[0])){
                    result = result / otherPair[1];
                }
            })
            if(result === goal) isRuleCompliant = true;
        })
        if(isRuleCompliant) this.addRuleSolutionIfRowColumnConditionHolds(ruleSolution);
    }

    private addRuleSolutionIfRowColumnConditionHolds(ruleSolution: RuleSolution) {
        let coordinateValueMap = ruleSolution.getAll();
        let coordinateValuePair;
        let coordinateValueArray = [];
        let coordinateInSameRowOrColumn: boolean = false;
        while(!(coordinateValuePair = coordinateValueMap.next()).done) {
            coordinateValueArray.push(coordinateValuePair.value);
        }
        coordinateValueArray.forEach((pair) => {
            coordinateValueArray.forEach((otherPair) => {
                if(pair[1]==otherPair[1]){
                    if(!pair[0].equals(otherPair[0])){
                        if(pair[0].x===otherPair[0].x) coordinateInSameRowOrColumn = true;
                        else if (pair[0].y===otherPair[0].y) coordinateInSameRowOrColumn = true;
                    }
                }
            })
        });
        if(!coordinateInSameRowOrColumn) this.ruleSolutions[this.ruleSolutionIndex].push(ruleSolution);
    }

}
