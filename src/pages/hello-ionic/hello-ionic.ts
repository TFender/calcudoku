import {Component} from '@angular/core';
import {AppService} from "../../app/app.service";
import {Coordinate} from "../../app/model/coordinate";
import {Rule} from "../../app/model/rule";
import {RuleType} from "../../app/model/rule-type";
import {AlertController, MenuController, Menu} from 'ionic-angular';
import {CoordinateWrapper} from "../../app/model/coordinate-wrapper";
import {RuleSolution} from "../../app/model/rule-solution";

@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {
  matrix: CoordinateWrapper[][];
  solution: number[][];
  displaySolutions: string[][] = [];
  tempString: string = "";
  amount: number;
  dimension: number;
  coveredCoordinates: CoordinateWrapper[] = [];
  ruleCoordinateWrappers: CoordinateWrapper[] = [];
  content: Menu;
  fontSize: number;
  bordersize: number;
  lock: boolean = false;

  constructor(private appService: AppService, private alertCtrl: AlertController, public menuCtrl: MenuController) {
    this.setMatrixWithDimension(4);
  }

  setAndRestGrid(dimension) {
    let prompt = this.alertCtrl.create({
      title: 'Grid',
      subTitle: "Set and Reset Grid to " + dimension + "x" + dimension,
      cssClass: 'alert-box-text',
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            this.setMatrixWithDimension(dimension);
          }
        },
        {
          text: 'No',
          handler: data => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    prompt.present();
  }

  setMatrixWithDimension(dimension: number) {
    this.dimension = dimension;
    this.fontSize = dimension;
    this.bordersize = dimension;
    this.matrix = [];

    this.appService.setEmptyMatrixWithGivenDimension(dimension);
    for (let i = 0; i < dimension; i++) {
      this.matrix.push([]);
      for (let j = 0; j < dimension; j++) {
        this.matrix[i].push(new CoordinateWrapper(new Coordinate(j + 1, i + 1)));
      }
    }
    this.displaySolutions = [];
    this.tempString = "";
    this.coveredCoordinates = [];
    this.ruleCoordinateWrappers = [];
  }

  toggleMenu(id: string) {
    this.menuCtrl.enable(true, 'mycontent');
    this.menuCtrl.toggle(id);
  }


  setTempSolutions(entry: CoordinateWrapper) {
    if (!this.lock) {
      this.lock = true;
      for (let column of this.matrix) {
        for (let entry of column) {
          entry.solutionOptions = [];
        }
      }
      let rs2: RuleSolution[] = [];
      this.appService.prepareRuleSolutions();
      for (let rsarray of this.appService.ruleSolutions) {
        console.log(this.appService.ruleSolutions.length);
        if (rsarray.length != 0) {
          for (let rs of rsarray) {
            if (rs.checkIfCoordinateExists(entry.coordinate)) {
              rs2.push(rs);
            }
          }
        }
        if (rsarray.length != 0) {
          entry.solutionOptions = [];
          for (let rs of rs2) {
            if (rs != null)
              entry.solutionOptions.push(rs.getAllValues());
          }
        }
      }
      this.sortOptions(entry.solutionOptions);
    }
    this.lock=false;
  }

  sortOptions(inputArray: string[]) {
    let solutions: string[] = [];
    if (inputArray.length == 0) {
      inputArray.push("None");
    }
    else {
      for (let option of inputArray) {
        let numArray: number[] = [];
        for (var i = 0; i < option.length; i++) {
          numArray.push(Number(option.charAt(i)))
        }
        numArray.sort((n1,n2) => n2 - n1);
        let s: string = "";
        for(let num of numArray){
          s+=""+num;
        }

        if (solutions.length == 0) {
          console.log(s)
          solutions.push(s);
        }
        else {
          if (solutions.indexOf(s) == -1) {
            solutions.push(s);
          }
        }
        solutions.sort((n1,n2) => {
          if (n1 > n2) {
            return -1;
          }
          if (n1 < n2) {
            return 1;
          }
          return 0;});
      }
      this.setDisplaySolutionArray(solutions);
      return;
    }
    this.setDisplaySolutionArray(inputArray);
  }

  setDisplaySolutionArray(inputArray: string[]) {
    this.displaySolutions = [];
    let evenHolderArray: string[] = [];
    let oddHolderArray: string[] = [];
    let sort: boolean = false;
    for (let numberInArray of inputArray) {
      if (sort) {
        evenHolderArray.push(numberInArray);
        sort = !sort;
        continue;
      }
      oddHolderArray.push(numberInArray);
      sort = !sort;
    }
    this.displaySolutions.push(oddHolderArray);
    this.displaySolutions.push(evenHolderArray);
  }

  getRuleType(operator: string): RuleType {
    if (operator == 'add') return RuleType.ADD;
    if (operator == 'single') return RuleType.SINGLE;
    if (operator == 'multiply') return RuleType.MULTIPLY;
    if (operator == 'divide') return RuleType.DIVIDE;
    if (operator == 'subtract') return RuleType.SUBTRACT;
  }

  pickAmount(color: string, ruleType: RuleType) {
    if ((ruleType == RuleType.SINGLE && this.ruleCoordinateWrappers.length == 1) || (ruleType != RuleType.SINGLE && this.ruleCoordinateWrappers.length > 1)) {
      let prompt = this.alertCtrl.create({
        subTitle: "Enter an Amount",
        cssClass: 'alert-box-text',
        inputs: [{
          name: 'Amount',
          type: 'number',
          placeholder: ''
        }],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'OK',
            handler: data => {
              this.amount = Number.parseInt(data.Amount);
              this.addMathRule(color, ruleType);
            }
          }
        ]
      });
      prompt.present();
    }
  }

  preSolve() {
    if (this.appService.allCoordinatesCovered) {
      let prompt = this.alertCtrl.create({
        title: 'Solve',
        subTitle: "Are you sure?",
        cssClass: 'alert-box-text',
        buttons: [
          {
            text: 'Yes',
            handler: data => {
              this.solve();
            }
          },
          {
            text: 'No',
            handler: data => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      prompt.present();
    } else {
      let prompt = this.alertCtrl.create({
        title: 'Not Yet',
        cssClass: 'alert-box-text',
        subTitle: "Must enter all coordinates",
        buttons: ['OK']
      });
      prompt.present();
    }
  }

  addCoordinateToRule(coordinateWrapper: CoordinateWrapper) {
    if (coordinateWrapper.clickable) {
      if (this.ruleCoordinateWrappers.indexOf(coordinateWrapper) === -1)
        this.ruleCoordinateWrappers.push(coordinateWrapper);
    }
  }

  removeCoordinateFromRule(coordinateWrapper: CoordinateWrapper) {
    if (coordinateWrapper.clickable) {
      if (this.ruleCoordinateWrappers.indexOf(coordinateWrapper) !== -1)
        this.ruleCoordinateWrappers.splice(this.ruleCoordinateWrappers.indexOf(coordinateWrapper), 1);
    }
  }

  solve() {
    this.appService.publishedSolution.subscribe(
      response => {
        if (response.length === 0) {
          this.alertBoxNoSolution();
        } else {
          this.solution = response;
          this.putSolutionInMatrixOnScreen();
        }
      },
      error => console.log(error),
      () => console.log("end")
    );
    this.appService.solve();
  }

  alertBoxNoSolution() {
    let prompt = this.alertCtrl.create({
      title: 'No solution',
      subTitle: "There is no solution.",
      cssClass: 'alert-box-text',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    prompt.present();
    this.setMatrixWithDimension(this.dimension);
  }

  putSolutionInMatrixOnScreen() {
    for (let i = 0; i < this.dimension; i++) {
      for (let j = 0; j < this.dimension; j++) {
        if (this.solution[j][i] != null) {
          this.matrix[i][j].text = "" + this.solution[j][i];
        }
      }
    }
  }

  addMathRule(color: string, ruleType: RuleType) {
    if ((this.amount !== null) && (this.amount !== undefined) && (this.ruleCoordinateWrappers != null)) {
      let ruleCoordinates = [];
      this.ruleCoordinateWrappers.forEach((entry) => ruleCoordinates.push(entry.coordinate));
      let rule: Rule;
      if (ruleType === RuleType.SINGLE) rule = new Rule(RuleType.ADD, this.amount, ruleCoordinates);
      else rule = new Rule(ruleType, this.amount, ruleCoordinates);
      this.setToColor(color, this.ruleCoordinateWrappers);
      this.displayRulesAndBorders(ruleType, this.amount, this.ruleCoordinateWrappers);
      this.appService.addRule(rule);
      if (ruleType == RuleType.SINGLE) this.ruleCoordinateWrappers[0].setAndDisplayNumber(this.amount);
      this.coveredCoordinates = this.coveredCoordinates.concat(this.ruleCoordinateWrappers);
      this.ruleCoordinateWrappers = [];
      this.amount = 0;
    }
  }

  reset() {
    this.ruleCoordinateWrappers = [];
    this.coveredCoordinates = [];
    for (let column of this.matrix) {
      for (let entry of column) {
        entry.reset();
      }
    }
  }

  setToColor(color: string, ruleCoordinates: CoordinateWrapper[]) {
    for (let coordinate of ruleCoordinates) {
      coordinate.setColor(color);
      coordinate.clickable = false;
      coordinate.unselect();
    }
  }

  displayRulesAndBorders(ruleType: RuleType, amount: number, ruleCoordinates: CoordinateWrapper[]) {
    this.displayRule(ruleType, amount, ruleCoordinates);
    this.displayBorder(ruleCoordinates);
  }

  displayRule(ruleType: RuleType, amount: number, ruleCoordinates: CoordinateWrapper[]) {
    var x = this.matrix.length;
    var y = this.matrix.length;
    for (let coordinateWrapper of ruleCoordinates) {
      if (coordinateWrapper.coordinate.y < y) {
        y = coordinateWrapper.coordinate.y;
      }
    }
    for (let coordinateWrapper of ruleCoordinates) {
      if (coordinateWrapper.coordinate.y == y) {
        if (coordinateWrapper.coordinate.x < x) {
          x = coordinateWrapper.coordinate.x;
        }
      }
    }
    for (let coordinateWrapper of ruleCoordinates) {
      if (coordinateWrapper.coordinate.y == y && coordinateWrapper.coordinate.x == x) {
        coordinateWrapper.rule = "" + amount + "" + this.getOperatorSymbol(ruleType);
      }
    }
  }

  displayBorder(ruleCoordinates: CoordinateWrapper[]) {

    for (let coordinateWrapper of ruleCoordinates) {
      coordinateWrapper.setBordersGrey();
      let left: boolean = false;
      let right: boolean = false;
      let top: boolean = false;
      let bottom: boolean = false;
      var x = coordinateWrapper.coordinate.x;
      var y = coordinateWrapper.coordinate.y;
      for (let neighbour of ruleCoordinates) {
        if (neighbour.coordinate.x == x && neighbour.coordinate.y == y) continue;
        if (neighbour.coordinate.x == x - 1 && neighbour.coordinate.y == y) left = true;
        if (neighbour.coordinate.x == x + 1 && neighbour.coordinate.y == y) right = true;
        if (neighbour.coordinate.x == x && neighbour.coordinate.y == y + 1) bottom = true;
        if (neighbour.coordinate.x == x && neighbour.coordinate.y == y - 1) top = true;
      }
      if (!left) coordinateWrapper.setBorderBlack('left');
      if (!right) coordinateWrapper.setBorderBlack('right');
      if (!bottom) coordinateWrapper.setBorderBlack('bottom');
      if (!top) coordinateWrapper.setBorderBlack('top');
    }
  }

  getOperatorSymbol(ruleType: RuleType): string {
    if (ruleType == RuleType.ADD) return "+";
    if (ruleType == RuleType.SUBTRACT) return "-";
    if (ruleType == RuleType.MULTIPLY) return "×";
    if (ruleType == RuleType.DIVIDE) return "÷";
    if (ruleType == RuleType.SINGLE) return "";
  }
}

